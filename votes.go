package main

type Vote struct {
	Rating uint   `json:"rating" binding:"required"`
	TalkId string `json:"talkId" binding:"required"`
	User   uint   `json:"user" binding:"required"`
}

/*
{
id: 51,
deviceId: "AC1FD953-5C7F-4B45-9DBF-4696A1857118",
conferenceId: 12,
rating: 4,
date: "2014-06-12T09:07:09.000Z",
presentationId: "4134"

}
*/
