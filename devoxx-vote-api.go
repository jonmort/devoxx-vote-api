package main

import (
	"flag"
	"fmt"
	"log"
	"net/http"
	"strconv"
	"strings"

	"github.com/gin-gonic/gin"
)

var requireOpen *bool

func CORSMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		c.Writer.Header().Set("Content-Type", "application/json")
		c.Writer.Header().Set("Access-Control-Allow-Origin", "*")
		c.Next()
	}
}

func main() {
	loadFromDb := flag.Bool("reload", false, "Reload talk and speaker data from REST API")
	requireOpen = flag.Bool("requireOpen", true, "Require talks to have started before they are eligible for voting")
	applyOffset := flag.Bool("applyOffset", false, "Apply and offset to the start times of each talk when read from the Devoxx UK API")
	bind := flag.String("bind", "0.0.0.0:8080", "Host to bind to")

	dbUrl := flag.String("dbUrl", "postgres://postgres:postgres@127.0.0.1:5432/vote?sslmode=disable", "Postgres DB URL")

	flag.Parse()

	log.Printf("Flags, loadFromDb: %t, requireOpen: %t, applyOffset: %t, bind: %s, dbUrl: %s", *loadFromDb, *requireOpen, *applyOffset, *bind, *dbUrl)

	if err := InitDb(*dbUrl); err != nil {
		log.Fatalf("Database Initialisation failed: %v", err)
	}
	defer db.Close()

	if *loadFromDb {
		PopulateDataFromRest(*applyOffset)
	} else {
		r := gin.Default()
		r.Use(CORSMiddleware())
		duk15 := r.Group("/DV15")

		// disable voting now the conference is over
		//duk15.POST("/vote", VoteEndpoint)
		duk15.GET("/top/talks", TopTalksEndpoint)
		duk15.GET("/categories", CategoriesEndpoint)
		duk15.GET("/talk/:talkId", TalkEndpoint)
		duk15.PUT("/talk/:talkId/youtubeURL", UpdateYoutubeURLEndpoint)

		r.Run(*bind)
	}
}

func GetLimit(c *gin.Context) int64 {
	limit := int64(10)
	if c.Request.URL.Query().Get("limit") != "" {
		l, err := strconv.ParseInt(c.Request.URL.Query().Get("limit"), 10, 0)

		if err != nil || l > 100 || l < 1 {
			c.JSON(http.StatusBadRequest, gin.H{"message": "limit must be a positive integer less than 100"})
			return -1
		}
		limit = l
	}
	return limit
}

func TopTalksEndpoint(c *gin.Context) {
	limit := GetLimit(c)
	if limit == -1 {
		return
	}
	query := c.Request.URL.Query()
	order, err := TopTalksQuery(limit, query.Get("day"), query.Get("talkType"), query.Get("track"))
	if err != nil {
		c.Error(err)

	} else {
		c.JSON(http.StatusOK, gin.H{"talks": order})
	}
}

func VoteEndpoint(c *gin.Context) {
	var vote Vote
	if err := c.BindJSON(&vote); err != nil {
		// a bad request will be set by gin
		log.Printf("Bad Request from %s", c.Request.Header.Get("User-Agent"))
		log.Println(err)
		return
	}

	log.Printf("Vote Submitted: rating: %d, talkId: %s, user: %d, Agent: %s", vote.Rating, vote.TalkId, vote.User, c.Request.Header.Get("User-Agent"))

	if *requireOpen {
		if open, err := IsVotingOpen(vote.TalkId); err == nil && !open {
			log.Printf("Can't vote on %s yet", vote.TalkId)
			c.JSON(http.StatusBadRequest, gin.H{"messaage": "Cannot vote on talk yet"})
			return
		} else if err != nil {
			c.Error(err)
			c.JSON(http.StatusInternalServerError, gin.H{"messaage": err.Error()})
			return
		}
	}

	if vote.Rating < 1 || vote.Rating > 5 {
		msg := fmt.Sprintf("Rating %d must be between 1 and 5", vote.Rating)
		log.Print(msg)
		c.JSON(http.StatusBadRequest, gin.H{"messaage": msg})
		return
	}

	ip := GetIpAddress(c)

	if err := LogVote(vote, ip); err != nil {
		log.Printf("Failed to record vote %v", vote)
		c.Error(err)
		c.JSON(http.StatusInternalServerError, gin.H{"message": err.Error()})
	}

	c.JSON(http.StatusCreated, vote)
}

func CategoriesEndpoint(c *gin.Context) {
	days, err := DaysQuery()
	if err != nil {
		c.Error(err)
		return
	}
	types, err := TypesQuery()
	if err != nil {
		c.Error(err)
		return
	}
	tracks, err := TracksQuery()
	if err != nil {
		c.Error(err)

	} else {
		c.JSON(http.StatusOK, gin.H{
			"days":      days,
			"talkTypes": types,
			"tracks":    tracks})
	}
}

func TalkEndpoint(c *gin.Context) {
	talk := c.Params.ByName("talkId")
	result, err := TalkQuery(talk)
	if err != nil {
		c.Error(err)

	} else if result != nil {
		c.JSON(http.StatusOK, result)
	} else {
		c.JSON(http.StatusNotFound, gin.H{"message": "No votes for talk"})
	}
}

func GetIpAddress(c *gin.Context) string {
	ip := strings.Split(c.ClientIP(), ":")[0]
	forwardedFor := c.Request.Header.Get("X-Forwarded-For")
	if forwardedFor != "" {
		ip = forwardedFor
	}
	return ip
}

func UpdateYoutubeURLEndpoint(c *gin.Context) {
	talkId := c.Params.ByName("talkId")
	youtubeURL := c.PostForm("youtubeURL")

	// TODO: Should we restrict in iteration0 to a fixed set of IP addresses? HTTP basic auth or simple API key?
	ip := GetIpAddress(c)
	log.Printf("Got request to set youtubeURL=%s for talkId=%s from IP=%s", youtubeURL, talkId, ip)

	result, err := setYoutubeURL(talkId, youtubeURL)
	if err != nil {
		c.Error(err)
	} else {
		c.JSON(http.StatusOK, result)
	}
}