-- clear everything out
TRUNCATE talk_by CASCADE;
TRUNCATE talk CASCADE;
TRUNCATE speaker CASCADE;

-- add the columns
ALTER TABLE talk
  ADD COLUMN title text NOT NULL,
  ADD COLUMN type text NOT NULL,
  ADD COLUMN track text NOT NULL,
  ADD COLUMN summary text NOT NULL;
