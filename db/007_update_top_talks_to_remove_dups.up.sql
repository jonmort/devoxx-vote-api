-- add the columns
DROP MATERIALIZED VIEW toptalks;

CREATE MATERIALIZED VIEW toptalks AS
  SELECT top.talkid,
    top.avg,
    top.count,
    top.sum,
    top.title,
    top.type,
    top.track,
    top.summary,
    top.day,
    speaker.speakers
  FROM
    (SELECT vote.talkId as talkid,
           avg(vote.rating) as avg,
           count(vote.rating) as count,
           sum(vote.rating) as sum,
           talk.title as title,
           talk.type as type,
           talk.track as track,
           talk.summary as summary,
           talk.day as day
      FROM unique_vote AS vote, talk
      WHERE vote.talkId = talk.talkId
      GROUP BY talk.talkId, vote.talkId
      ORDER BY avg DESC) as top,
    (SELECT talk_by.talkId,  array_agg(distinct speaker.name) as speakers
     FROM talk_by, speaker
     WHERE talk_by.speakerId = speaker.uuid
     GROUP BY talk_by.talkid) as speaker
    WHERE speaker.talkId = top.talkid
    ORDER BY top.avg DESC;
